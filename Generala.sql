USE [master]
GO
/****** Object:  Database [Generala]    Script Date: 18/11/2020 04:28:54 ******/
CREATE DATABASE [Generala]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Generala', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Generala.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Generala_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Generala_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Generala] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Generala].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Generala] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Generala] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Generala] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Generala] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Generala] SET ARITHABORT OFF 
GO
ALTER DATABASE [Generala] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Generala] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Generala] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Generala] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Generala] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Generala] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Generala] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Generala] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Generala] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Generala] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Generala] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Generala] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Generala] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Generala] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Generala] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Generala] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Generala] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Generala] SET  MULTI_USER 
GO
ALTER DATABASE [Generala] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Generala] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Generala] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Generala] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Generala] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Generala] SET QUERY_STORE = OFF
GO
USE [Generala]
GO
/****** Object:  Table [dbo].[Jugadas]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugadas](
	[idPartida] [int] NOT NULL,
	[jugador] [varchar](50) NOT NULL,
	[juego] [varchar](50) NOT NULL,
	[puntaje] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jugador]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugador](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[mail] [varchar](50) NULL,
	[usuario] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[activo] [int] NULL,
	[fechaRegistro] [datetime] NULL,
 CONSTRAINT [PK_Jugador] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Partida]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partida](
	[id] [int] NOT NULL,
	[jugador1] [varchar](50) NULL,
	[jugador2] [varchar](50) NULL,
	[fechaInicio] [datetime] NULL,
	[tiempo] [float] NULL,
	[ganador] [varchar](50) NULL,
	[fechaFinalizacion] [datetime] NULL,
 CONSTRAINT [PK_Partida] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sesion]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sesion](
	[id] [int] NOT NULL,
	[jugador] [varchar](50) NULL,
	[fecha] [date] NULL,
	[observacion] [varchar](50) NULL,
 CONSTRAINT [PK_Sesion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[BloquearUsuario]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BloquearUsuario]
@usuario varchar(50)
as
begin

	update Jugador
	set activo = 0
	where usuario like @usuario

end
GO
/****** Object:  StoredProcedure [dbo].[InicializarPartida]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InicializarPartida]
@jugador1 varchar(50), @jugador2 varchar(50)
as
begin

	declare @id int
	declare @fechaInicio datetime
	declare @fechaFinalizacion datetime
	declare @tiempo int
	set @id = (select ISNULL( MAX(p.id) , 0) + 1 from Partida p)
	set @fechaInicio = GETDATE()
	set @fechaFinalizacion = GETDATE()
	set @tiempo = 0

	insert into Partida (id, jugador1,jugador2, fechaInicio, fechaFinalizacion,tiempo) values (@id,@jugador1,@jugador2,@fechaInicio,@fechaFinalizacion,@tiempo)

	return @id

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarJugada]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[InsertarJugada]
@id int, @jugador varchar(50), @juego varchar(50), @puntaje int
as
begin

	insert into Jugadas (idPartida,jugador,juego,puntaje) values (@id,@jugador,@juego,@puntaje)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarJugador]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarJugador]
@nombre varchar(50), @apellido varchar(50), @mail varchar(50),@usuario varchar(50),@password varchar(50)
as
begin

	declare @id int
	declare @fechaRegistro DateTime
	declare @activo int

	set @id = (select ISNULL( MAX(e.id) , 0) + 1 from Jugador e)
	set @fecharegistro = GETDATE()
	set @activo = 1

	insert into Jugador (id, Nombre, Apellido, mail, usuario, password, fechaRegistro, activo) values (@id,@nombre,@apellido,@mail, @usuario, @password, @fechaRegistro, @activo)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarSesion]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarSesion]
@jugador varchar(50), @observacion varchar(50)
as
begin
	
	declare @id int
	declare @fecha datetime

	set @id = (select ISNULL( MAX(s.id) , 0) + 1 from Sesion s)
	set @fecha = GETDATE()

	insert into Sesion (id, jugador, fecha, observacion) values (@id,@jugador,@fecha,@observacion)

end
GO
/****** Object:  StoredProcedure [dbo].[ListarJugadas]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarJugadas]
as
begin

	select j.idPartida, j.jugador,j.juego,j.puntaje from Jugadas j

end
GO
/****** Object:  StoredProcedure [dbo].[ListarJugadores]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarJugadores]
as
begin

	select j.id, j.nombre, j.apellido, j.mail, j.usuario, j.fechaRegistro, j.activo from Jugador j

end

GO
/****** Object:  StoredProcedure [dbo].[ListarPartidas]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarPartidas]
as
begin

	select p.id,p.jugador1,p.jugador2,p.fechaInicio, p.fechaFinalizacion,p.tiempo,p.ganador from Partida p

end
GO
/****** Object:  StoredProcedure [dbo].[ObtenerJugador]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ObtenerJugador]
@usuario varchar(50)
as
begin
	select j.id, j.nombre, j.apellido, j.usuario, j.mail, j.fechaRegistro, j.activo from Jugador j
	where j.usuario = @usuario
end
GO
/****** Object:  StoredProcedure [dbo].[RegistrarPartida]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[RegistrarPartida]
@tiempo int, @ganador varchar(50)
as
begin
	
	declare @fechaFinalizacion datetime
	set @fechaFinalizacion = GETDATE()

	declare @id int
	set @id = (select TOP 1 p.id from Partida p order by id desc)

	update Partida set
	fechaFinalizacion = @fechaFinalizacion,
	tiempo = @tiempo,
	ganador = @ganador
	where id = @id

	select @tiempo as resultado

end
GO
/****** Object:  StoredProcedure [dbo].[ValidarUsuario]    Script Date: 18/11/2020 04:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ValidarUsuario]
@usuario varchar(50), @password varchar(50)
as
begin


		declare @cantidad int
		declare @activo int

		set @cantidad = (select COUNT(j.usuario) from Jugador j where j.usuario = @usuario)

		if @cantidad = 1
			begin
				set @cantidad = (select COUNT(j.password) from Jugador j where j.usuario = @usuario and j.password = @password)
				set @activo = (select j.activo from Jugador j where j.usuario = @usuario and j.password = @password)
		
			if @cantidad = 0 --La contraseña esta mal 
				begin
					select -1 as resultado
				end
			else if @activo = 0
				begin
					select 0 as resultado --El usuario esta bloqueado
				end
			else
				begin
					select 1 as resultado --Esta todo ok
				end
			end
		else
			begin
				select -2 as resultado --No existe el usuario
			end
	end
GO
USE [master]
GO
ALTER DATABASE [Generala] SET  READ_WRITE 
GO
