﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class Acceso
    {
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection("Initial catalog=Generala; Data Source=.\\Sqlexpress; Integrated Security=SSPI");
            conexion.Open();

        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string SQL, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {

            SqlCommand comando = new SqlCommand(SQL);

            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            comando.Connection = conexion;

            return comando;

        }
        public int Escribir(string sql, List<SqlParameter> parametros = null)
        {
            Abrir();

            SqlCommand comando = CrearComando(sql, parametros, CommandType.StoredProcedure);

            
            int filasAfectadas = 0;

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message.ToString());

                filasAfectadas = -1;
            }

            Cerrar();

            return filasAfectadas;
        }

        public DataTable Leer(string sql, List<SqlParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql, parametros, CommandType.StoredProcedure);

            DataTable tabla = new DataTable();

            adaptador.Fill(tabla);

            return tabla;

        }
    }
}