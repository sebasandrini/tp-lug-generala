﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Dado
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private bool tirar = true;

        public bool Tirar
        {
            get { return tirar; }
            set { tirar = value; }
        }


        public void GenerarNumero(Random num)
        {
            numero = num.Next(1, 7);
        }

        public override string ToString()
        {
            return numero.ToString();
        }
    }
}