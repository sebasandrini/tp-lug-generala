﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Xml;
using System.Xml.Linq;

namespace BLL
{
    public class Jugada
    {
        private int idPartida;

        public int IdPartida
        {
            get { return idPartida; }
            set { idPartida = value; }
        }

        private string jugador;

        public string Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private string juego;

        public string Juego
        {
            get { return juego; }
            set { juego = value; }
        }

        private int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
            set { puntaje = value; }
        }

        public static int InsertarJugada(Jugada jugada, Partida partida)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter idPartida = new SqlParameter();
            idPartida.ParameterName = "@id";
            idPartida.Value = partida.ID;
            idPartida.SqlDbType = SqlDbType.Int;
            parametros.Add(idPartida);

            SqlParameter jugador = new SqlParameter();
            jugador.ParameterName = "@jugador";
            jugador.Value = jugada.Jugador;
            jugador.SqlDbType = SqlDbType.Text;
            parametros.Add(jugador);

            SqlParameter juego = new SqlParameter();
            juego.ParameterName = "@juego";
            juego.Value = jugada.juego;
            juego.SqlDbType = SqlDbType.Text;
            parametros.Add(juego);

            SqlParameter puntaje = new SqlParameter();
            puntaje.ParameterName = "@puntaje";
            puntaje.Value = jugada.puntaje;
            puntaje.SqlDbType = SqlDbType.Int;
            parametros.Add(puntaje);

            string SQL = "InsertarJugada";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public static List<Jugada> ListarJugadas()
        {
            List<Jugada> jugadas = new List<Jugada>();

            Acceso acceso = new Acceso();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarJugadas");

            foreach (DataRow registro in tabla.Rows)
            {
                Jugada jugada = new Jugada();

                jugada.idPartida = int.Parse(registro["idPartida"].ToString());
                jugada.jugador = registro["jugador"].ToString();
                jugada.juego = registro["juego"].ToString();
                jugada.puntaje = int.Parse(registro["puntaje"].ToString());

                jugadas.Add(jugada);
            }

            acceso.Cerrar();

            return jugadas;
        }
    }
}