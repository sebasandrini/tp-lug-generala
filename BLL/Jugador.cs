﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
    public class Jugador
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string mail;

        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }

        private DateTime fechaRegistro;

        public DateTime FechaRegistro
        {
            get { return fechaRegistro; }
            set { fechaRegistro = value; }
        }

        private int activo;

        public int Activo
        {
            get { return activo; }
        }



        public List<Dado> Tirar(List<Dado> dados, Random num)
        {
            if (dados.Count>0)
            {
                foreach (Dado dado in dados)
                {
                    if (dado.Tirar == true)
                    {
                        dado.GenerarNumero(num);
                        dado.Tirar = false;
                    }
                    
                }
            }

            return dados;
            
        }

        public static int InsertarJugador(string nom, string ape, string correo, string user, string pass)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nombre";
            nombre.Value = nom;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@apellido";
            apellido.Value = ape;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);

            SqlParameter mail = new SqlParameter();
            mail.ParameterName = "@mail";
            mail.Value = correo;
            mail.SqlDbType = SqlDbType.Text;
            parametros.Add(mail);

            SqlParameter usuario = new SqlParameter();
            usuario.ParameterName = "@usuario";
            usuario.Value = user;
            usuario.SqlDbType = SqlDbType.Text;
            parametros.Add(usuario);

            SqlParameter password = new SqlParameter();
            password.ParameterName = "@password";
            password.Value = pass;
            password.SqlDbType = SqlDbType.Text;
            parametros.Add(password);

            string SQL = "InsertarJugador";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public static List<Jugador> ListarTodos()
        {
            List<Jugador> jugadores = new List<Jugador>();

            Acceso acceso = new Acceso();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarJugadores");

            foreach (DataRow registro in tabla.Rows)
            {
                Jugador jugador = new Jugador();

                jugador.id = int.Parse(registro["id"].ToString());
                jugador.nombre = registro["nombre"].ToString();
                jugador.apellido = registro["apellido"].ToString();
                jugador.mail = registro["mail"].ToString();
                jugador.usuario = registro["usuario"].ToString();
                jugador.fechaRegistro = DateTime.Parse(registro["fechaRegistro"].ToString());
                jugador.activo = int.Parse(registro["activo"].ToString());

                jugadores.Add(jugador);
            }

            acceso.Cerrar();

            return jugadores;
        }

        public static int ValidarJugador(string user, string pass)
        {
            int resultado = 0;

            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter usuario = new SqlParameter();
            usuario.ParameterName = "@usuario";
            usuario.Value = user;
            usuario.SqlDbType = SqlDbType.Text;
            parametros.Add(usuario);

            SqlParameter password = new SqlParameter();
            password.ParameterName = "@password";
            password.Value = pass;
            password.SqlDbType = SqlDbType.Text;
            parametros.Add(password);

            acceso.Abrir();

            string SQL = "ValidarUsuario";

            DataTable tabla = acceso.Leer(SQL, parametros);

            resultado = int.Parse(tabla.Rows[0]["resultado"].ToString());

            acceso.Cerrar();

            return resultado;
        }

        public static void BloquearUsuario(string user)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter usuario = new SqlParameter();
            usuario.ParameterName = "@usuario";
            usuario.Value = user;
            usuario.SqlDbType = SqlDbType.Text;
            parametros.Add(usuario);

            string SQL = "BloquearUsuario";

            int resultado = acceso.Escribir(SQL, parametros);

        }

        public static Jugador ObtenerJugador(string user)
        {
            Acceso acceso = new Acceso();

            Jugador jugador = new Jugador();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter usuario = new SqlParameter();
            usuario.ParameterName = "@usuario";
            usuario.Value = user;
            usuario.SqlDbType = SqlDbType.Text;
            parametros.Add(usuario);

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ObtenerJugador", parametros);

            foreach (DataRow registro in tabla.Rows)
            {
                jugador.id = int.Parse(registro["id"].ToString());
                jugador.nombre = registro["nombre"].ToString();
                jugador.apellido = registro["apellido"].ToString();
                jugador.mail = registro["mail"].ToString();
                jugador.usuario = registro["usuario"].ToString();
                jugador.fechaRegistro = DateTime.Parse(registro["fechaRegistro"].ToString());
                jugador.activo = int.Parse(registro["activo"].ToString());
            }

            acceso.Cerrar();

            return jugador;
        }

        public override string ToString()
        {
            return nombre + " " + apellido;
        }
    }
}