﻿namespace BLL
{
    partial class CeldaGrilla
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.jugador1 = new System.Windows.Forms.Label();
            this.jugador2 = new System.Windows.Forms.Label();
            this.btnPuntaje = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // jugador1
            // 
            this.jugador1.AutoSize = true;
            this.jugador1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jugador1.ForeColor = System.Drawing.Color.White;
            this.jugador1.Location = new System.Drawing.Point(10, 13);
            this.jugador1.Name = "jugador1";
            this.jugador1.Size = new System.Drawing.Size(17, 17);
            this.jugador1.TabIndex = 1;
            this.jugador1.Text = "0";
            // 
            // jugador2
            // 
            this.jugador2.AutoSize = true;
            this.jugador2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jugador2.ForeColor = System.Drawing.Color.White;
            this.jugador2.Location = new System.Drawing.Point(117, 13);
            this.jugador2.Name = "jugador2";
            this.jugador2.Size = new System.Drawing.Size(17, 17);
            this.jugador2.TabIndex = 2;
            this.jugador2.Text = "0";
            // 
            // btnPuntaje
            // 
            this.btnPuntaje.BackColor = System.Drawing.Color.Transparent;
            this.btnPuntaje.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnPuntaje.Location = new System.Drawing.Point(37, 3);
            this.btnPuntaje.Name = "btnPuntaje";
            this.btnPuntaje.Size = new System.Drawing.Size(65, 34);
            this.btnPuntaje.TabIndex = 3;
            this.btnPuntaje.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnPuntaje.UseVisualStyleBackColor = false;
            this.btnPuntaje.Visible = false;
            this.btnPuntaje.Click += new System.EventHandler(this.btnPuntaje2_Click);
            // 
            // CeldaGrilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.btnPuntaje);
            this.Controls.Add(this.jugador2);
            this.Controls.Add(this.jugador1);
            this.Name = "CeldaGrilla";
            this.Size = new System.Drawing.Size(143, 41);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label jugador1;
        private System.Windows.Forms.Label jugador2;
        private System.Windows.Forms.Button btnPuntaje;
    }
}
