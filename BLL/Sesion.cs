﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
    public class Sesion
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private string observacion;

        public string Observacion
        {
            get { return observacion; }
            set { observacion = value; }
        }

        public static int InsertarSesion(Jugador j, string obs)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter jugador = new SqlParameter();
            jugador.ParameterName = "@jugador";
            jugador.Value = j.Usuario;
            jugador.SqlDbType = SqlDbType.Text;
            parametros.Add(jugador);

            SqlParameter observacion = new SqlParameter();
            observacion.ParameterName = "@observacion";
            observacion.Value = obs;
            observacion.SqlDbType = SqlDbType.Text;
            parametros.Add(observacion);

            string SQL = "InsertarSesion";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }
    }
}