﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Tablero
    {
        public delegate void delEnviarCasillero(Casillero casillero);

        public event delEnviarCasillero EnviarCasillero;

        private List<Casillero> casilleros = new List<Casillero>();

        public List<Casillero> Casilleros
        {
            get { return casilleros; }
            set { casilleros = value; }
        }

        public void InicializarTablero()
        {
            for (int fila=0;fila<5;fila++)
            {
                for (int columna=0;columna<1;columna++)
                {
                    Casillero casillero = new Casillero();
                    casillero.X = fila;
                    casillero.Y = 120;
                    casillero.Numero = fila + 1;

                    this.EnviarCasillero(casillero);

                    casilleros.Add(casillero);
                }
            }
        }
    }
}