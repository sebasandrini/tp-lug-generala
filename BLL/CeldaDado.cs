﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace BLL
{
    public partial class CeldaDado : UserControl
    {
        public CeldaDado()
        {
            InitializeComponent();
        }

        SoundPlayer seleccionDado = new SoundPlayer(".\\Recursos\\seleccionDado.wav");
        SoundPlayer deseleccionDado = new SoundPlayer(".\\Recursos\\deseleccionDado.wav");

        private Casillero casillero;

        public Casillero Casillero
        {
            get { return casillero; }
            set { casillero = value; }
        }

        private Dado dado;

        public Dado Dado
        {
            get { return dado; }
            set { dado = value; }
        }

        public void Ocultar()
        {
            pictureBox3.Visible = false;
        }

        public void ReiniciarTablero()
        {
            pictureBox3.Visible = false;
            pictureBox2.Visible = false;
        }

        public void EstablecerImagen(int valor)
        {
            switch (valor)
            {
                case 1:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\1.jpg");
                        break;
                    }
                case 2:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\2.jpg");
                        break;
                    }
                case 3:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\3.jpg");
                        break;
                    }
                case 4:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\4.jpg");
                        break;
                    }
                case 5:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\5.jpg");
                        break;
                    }
                case 6:
                    {
                        pictureBox2.Visible = true;
                        pictureBox2.Image = Image.FromFile(".\\Recursos\\6.jpg");
                        break;
                    }
                default:
                    {
                        pictureBox1.Image = Image.FromFile(".\\Recursos\\7.jpg");
                        
                        break;
                    }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            dado.Tirar = true;
            seleccionDado.Play();
            pictureBox3.Visible = true;
            pictureBox3.BringToFront();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            dado.Tirar = false;
            deseleccionDado.Play();
            pictureBox3.Visible = false;
            pictureBox3.BringToFront();
        }
    }
}
