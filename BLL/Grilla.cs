﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Grilla
    {
        public delegate void delEnviarCasilleroGrilla(Casillero_Grilla casilleroGrilla);

        public event delEnviarCasilleroGrilla EnviarCasilleroGrilla;

        private List<Casillero_Grilla> casilleros = new List<Casillero_Grilla>();

        public List<Casillero_Grilla> Casilleros
        {
            get { return casilleros; }
            set { casilleros = value; }
        }

        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private bool uno = false;

        public bool Uno
        {
            get { return uno; }
            set { uno = value; }
        }

        private bool dos = false;

        public bool Dos
        {
            get { return dos; }
            set { dos = value; }
        }

        private bool tres = false;

        public bool Tres
        {
            get { return tres; }
            set { tres = value; }
        }

        private bool cuatro = false;

        public bool Cuatro
        {
            get { return cuatro; }
            set { cuatro = value; }
        }

        private bool cinco = false;

        public bool Cinco
        {
            get { return cinco; }
            set { cinco = value; }
        }

        private bool seis = false;

        public bool Seis
        {
            get { return seis; }
            set { seis = value; }
        }

        private bool escalera = false;

        public bool Escalera
        {
            get { return escalera; }
            set { escalera = value; }
        }

        private bool full = false;

        public bool Full
        {
            get { return full; }
            set { full = value; }
        }

        private bool poker = false;

        public bool Poker
        {
            get { return poker; }
            set { poker = value; }
        }

        private bool generala = false;

        public bool Generala
        {
            get { return generala; }
            set { generala = value; }
        }

        private bool generalaDoble = false;

        public bool GeneralaDoble
        {
            get { return generalaDoble; }
            set { generalaDoble = value; }
        }

        private int total = 0;

        public int Total
        {
            get { return total; }
            set { total = value; }
        }

        public void InicializarGrilla()
        {
            for (int fila = 0; fila < 11; fila++)
            {
                for (int columna = 0; columna < 1; columna++)
                {
                    Casillero_Grilla casilleroGrilla = new Casillero_Grilla();
                    casilleroGrilla.X = 750;
                    casilleroGrilla.Y = fila;
                    casilleroGrilla.Numero = fila + 1;

                    this.EnviarCasilleroGrilla(casilleroGrilla);

                    casilleros.Add(casilleroGrilla);
                }
            }

        }

        public void AnotarJuego (int numero)
        {
            if (numero == 1)
            {
                uno = !uno;
            }
            else if (numero == 2)
            {
                dos = !dos;
            }
            else if (numero == 3)
            {
                tres = !tres;
            }
            else if (numero == 4)
            {
                cuatro = !cuatro;
            }
            else if (numero == 5)
            {
                cinco = !cinco;
            }
            else if (numero == 6)
            {
                seis = !seis;
            }
            else if (numero == 7)
            {
                escalera = !escalera;
            }
            else if (numero == 8)
            {
                full = !full;
            }
            else if (numero == 9)
            {
                poker = !poker;
            }
            else if (numero == 10)
            {
                generala = !generala;
            }
            else
            {
                generalaDoble = !generalaDoble;
            }
        }
    }
}