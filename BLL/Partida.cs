﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Data.SqlClient;

namespace BLL
{
    public class Partida
    {

        public delegate void delEnviarPuntaje(int puntaje, int numero);

        public event delEnviarPuntaje EnviarPuntaje;

        public Partida()
        {
            fechaInicio = DateTime.Now;
            tiempo = 0;
            tiro = 3;
            turno = true;
            tirosPartida = 0;
        }

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private DateTime fechaInicio = new DateTime();

        public DateTime FechaInicio
        {
            get { return fechaInicio; }
            set { fechaInicio = value; }
        }

        private DateTime fechaFinalizacion = new DateTime();

        public DateTime FechaFinalizacion
        {
            get { return fechaFinalizacion; }
            set { fechaFinalizacion = value; }
        }


        private int tiempo;

        public int Tiempo
        {
            get { return tiempo; }
            set { tiempo = value; }
        }

        private Jugador jugador1;

        public Jugador Jugador1
        {
            get { return jugador1; }
            set { jugador1 = value; }
        }

        private Jugador jugador2;

        public Jugador Jugador2
        {
            get { return jugador2; }
            set { jugador2 = value; }
        }

        private bool turno;

        public bool Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        private int tiro;

        public int Tiro
        {
            get { return tiro; }
            set { tiro = value; }
        }

        private int tirosPartida;

        public int TirosPartida
        {
            get { return tirosPartida; }
            set { tirosPartida = value; }
        }

        private Jugador ganador;

        public Jugador Ganador
        {
            get { return ganador; }
            set { ganador = value; }
        }


        public int[] ArmarVector(List<Dado> dados)
        {
            int[] numeros = new int[7];

            for (int i = 0; i < 5; i++)
            {
                if (dados[i].Numero == 1)
                {
                    numeros[1]++;
                }
                else if (dados[i].Numero == 2)
                {
                    numeros[2]++;
                }
                else if (dados[i].Numero == 3)
                {
                    numeros[3]++;
                }
                else if (dados[i].Numero == 4)
                {
                    numeros[4]++;
                }
                else if (dados[i].Numero == 5)
                {
                    numeros[5]++;
                }
                else if (dados[i].Numero == 6)
                {
                    numeros[6]++;
                }
            }

            return numeros;
        }

        public void Generala(int[] dados, int tiro)
        {
            int resultado = -1;

            for (int i = 1;i<=6;i++)
            {
                if (dados[i] == 5)
                {
                    resultado = 50 + (3 * tiro);
                    break;
                }
            }

            this.EnviarPuntaje(resultado, 10);

        }

        public void GeneralaDoble(int[] dados, int tiro, bool tieneGenerala = false)
        {
            int resultado = -1;

            for (int i = 1; i <= 6; i++)
            {
                if (dados[i] == 5)
                {
                    if (tieneGenerala)
                    {
                        resultado = 100 + (3 * tiro);
                    }
                    break;
                }
            }

            this.EnviarPuntaje(resultado, 11);

        }

        public void Poker (int[] dados, int tiro)
        {
            int resultado = -1;

            for (int i = 1; i <= 6; i++)
            {
                if (dados[i] == 4)
                {
                    resultado = 40 + (3 * tiro);
                    break;
                }
            }

            this.EnviarPuntaje(resultado, 9);
            
        }

        public void Full (int[] dados, int tiro)
        {
            int resultado;
            
            bool tres = false, dos = false;

            for (int i = 1; i <= 6; i++)
            {
                if (dados[i] == 3)
                {
                    tres = true;
                    break;
                }
            }

            if (tres)
            {
                for (int i = 1; i <= 6; i++)
                {
                    if (dados[i] == 2)
                    {
                        dos = true;
                        break;
                    }
                }
            }

            if (tres && dos)
            {
                resultado = 30 + (3 * tiro);
            }
            else
            {
                resultado = -1;
            }

            this.EnviarPuntaje(resultado,8);
        }

        public void Escalera (int[] dados, int tiro)
        {
            int resultado = -1;
            

           if (dados[1] == 1 && dados[2] == 1 && dados[3] == 1 && dados[4] == 1 && dados[5] == 1)
            {
                resultado = 20 + (3 * tiro);
            }
            else if (dados[1] == 1 && dados[3] == 1 && dados[4] == 1 && dados[5] == 1 && dados[6] == 1)
            {
                resultado = 20 + (3 * tiro);
            }
            else if (dados[2] == 1 && dados[3] == 1 && dados[4] == 1 && dados[5] == 1 && dados[6] == 1)
            {
                resultado = 20 + (3 * tiro);
            }

            this.EnviarPuntaje(resultado, 7);     
        }

        public void LlenarTablero(int[] numeros, int tiro, Grilla grilla)
        {
            #region Numeros
            if (!grilla.Uno)
            {
                this.EnviarPuntaje(1 * numeros[1], 1);
            }
            else
            {
                this.EnviarPuntaje(-2, 1);
            }
            if (!grilla.Dos)
            {
                this.EnviarPuntaje(2 * numeros[2], 2);
            }
            else
            {
                this.EnviarPuntaje(-2, 2);
            }
            if (!grilla.Tres)
            {
                this.EnviarPuntaje(3 * numeros[3], 3);
            }
            else
            {
                this.EnviarPuntaje(-2, 3);
            }
            if (!grilla.Cuatro)
            {
                this.EnviarPuntaje(4 * numeros[4], 4);
            }
            else
            {
                this.EnviarPuntaje(-2, 4);
            }
            if (!grilla.Cinco)
            {
                this.EnviarPuntaje(5 * numeros[5], 5);
            }
            else
            {
                this.EnviarPuntaje(-2, 5);
            }
            if (!grilla.Seis)
            {
                this.EnviarPuntaje(6 * numeros[6], 6);
            }
            else
            {
                this.EnviarPuntaje(-2, 6);
            }
            #endregion

            #region Calculo de Juegos

            #region Escalera
            if (!grilla.Escalera)
            {
                Escalera(numeros, tiro);
            }
            #endregion

            #region Full
            if (!grilla.Full)
            {
                Full(numeros, tiro);
            }
            #endregion

            #region Poker
            if (!grilla.Poker)
            {
                Poker(numeros, tiro);
            }
            #endregion

            #region Generala
            if (!grilla.Generala)
            {
                Generala(numeros, tiro);
            }
            #endregion

            #region Generala doble
            if (!grilla.GeneralaDoble)
            {
                GeneralaDoble(numeros, tiro, grilla.Generala);
            }
            #endregion
            #endregion
        }

        public void CambiarTurno()
        {
            tirosPartida++;
            tiro = 3;
            this.turno = !this.turno;
        }

        public void RegistrarTiempo()
        {
            this.tiempo = DateTime.Now.Minute - this.fechaInicio.Minute;
        }

        public static int RegistrarPartida(Partida partida, string winner)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter tiempo = new SqlParameter();
            tiempo.ParameterName = "@tiempo";
            tiempo.Value = DateTime.Now.Minute - partida.fechaInicio.Minute;
            tiempo.SqlDbType = SqlDbType.Int;
            parametros.Add(tiempo);

            SqlParameter ganador = new SqlParameter();
            ganador.ParameterName = "@ganador";
            ganador.Value = winner;
            ganador.SqlDbType = SqlDbType.Text;
            parametros.Add(ganador);

            string SQL = "RegistrarPartida";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public static int InicializarPartida(string user1, string user2)
        {
            Acceso acceso = new Acceso();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter jugador1 = new SqlParameter();
            jugador1.ParameterName = "@jugador1";
            jugador1.Value = user1;
            jugador1.SqlDbType = SqlDbType.Text;
            parametros.Add(jugador1);

            SqlParameter jugador2 = new SqlParameter();
            jugador2.ParameterName = "@jugador2";
            jugador2.Value = user2;
            jugador2.SqlDbType = SqlDbType.Text;
            parametros.Add(jugador2);

            string SQL = "InicializarPartida";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public static List<Partida> ListarPartidas()
        {
            List<Partida> partidas = new List<Partida>();

            Acceso acceso = new Acceso();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarPartidas");

            foreach (DataRow registro in tabla.Rows)
            {
                Partida partida = new Partida();

                partida.id = int.Parse(registro["id"].ToString());
                partida.fechaInicio = DateTime.Parse(registro["fechaInicio"].ToString());
                partida.fechaFinalizacion = DateTime.Parse(registro["fechaFinalizacion"].ToString());
                partida.tiempo = int.Parse(registro["tiempo"].ToString());

                foreach (Jugador j in Jugador.ListarTodos())
                {
                    if (j.Usuario == registro["jugador1"].ToString())
                    {
                        partida.jugador1 = j;
                    }
                    else if (j.Usuario == registro["jugador2"].ToString())
                    {
                        partida.jugador2 = j;
                    }
                    if (j.Usuario == registro["ganador"].ToString())
                    {
                        partida.ganador = j;
                    }
                }
                partidas.Add(partida);
            }

            acceso.Cerrar();

            return partidas;
        }
    }
}