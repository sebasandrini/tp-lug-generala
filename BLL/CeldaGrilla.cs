﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

namespace BLL
{
    public partial class CeldaGrilla : UserControl
    {
        public delegate void delEnviarPuntaje(int puntaje, int juego);

        public event delEnviarPuntaje EnviarPuntaje;

        public CeldaGrilla()
        {
            InitializeComponent();
        }

        private int puntaje1;

        public int Puntaje1
        {
            get { return puntaje1; }
            set { puntaje1 = int.Parse(jugador1.Text); }
        }

        private int puntaje2;

        public int Puntaje2
        {
            get { return puntaje2; }
            set { puntaje2 = int.Parse(jugador2.Text); }
        }

        private bool turno;

        public bool Turno
        {
            get { return turno; }
            set { turno = value; }
        }


        private Casillero_Grilla casillero;

        public Casillero_Grilla Casillero
        {
            get { return casillero; }
            set { casillero = value; }
        }



        private void btnPuntaje2_Click(object sender, EventArgs e)
        {
            GrabarPuntaje(turno);
        }

        public void MostrarPuntaje (int puntaje)
        {

            if (puntaje != -1 && puntaje != -2)
            {
                btnPuntaje.Text = puntaje.ToString();
                btnPuntaje.Visible = true;

            }
            else if (puntaje == -1)
            {
                btnPuntaje.Text = 0.ToString();
                btnPuntaje.Visible = true;
            }
            else
            {
                btnPuntaje.Visible = false;
            }
            
        }

        public void GrabarPuntaje(bool turno)
        {
            if (turno)
            {
                jugador1.Text = btnPuntaje.Text;
                puntaje1 = int.Parse(btnPuntaje.Text);
                
            }
            else
            {
                jugador2.Text = btnPuntaje.Text;
                puntaje2 = int.Parse(btnPuntaje.Text);
            }

            this.EnviarPuntaje(int.Parse(btnPuntaje.Text), casillero.Numero);
            

        }

        public void ReiniciarGrilla()
        {
            btnPuntaje.Text = "";
            btnPuntaje.Visible = false;
            this.turno = !this.turno;
        }
    }
}
