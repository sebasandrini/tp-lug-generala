﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace TP_LUG___Generala
{
    public partial class AltaJugador : Form
    {
        public AltaJugador()
        {
            InitializeComponent();
        }

        List<Jugador> jugadores = new List<Jugador>();

        private void btnRegistrarse_Click(object sender, EventArgs e)
        {
            if(ValidarCampos())
            {
                if (ExisteJugador(jugadores, txtUsuario.Text, txtMail.Text) == 1)
                {
                    int resultado = 0;
                    resultado = Jugador.InsertarJugador(txtNombre.Text, txtApellido.Text, txtMail.Text, txtUsuario.Text, txtPassword.Text);

                    if (resultado > 0)
                    {
                        LimpiarCampos();
                        MessageBox.Show("Jugador registrado con éxito");
                    }
                    //Sino muestro un mensaje de error
                    else
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                }
                else if(ExisteJugador(jugadores, txtUsuario.Text, txtMail.Text) == -1)
                {
                    MessageBox.Show("Ya hay un jugador registrado con ese usuario\nPor favor, elegir otro usuario o seleccionar \"Olvide mi contraseña\"");
                    txtUsuario.Clear();
                    txtPassword.Clear();
                    txtRepetirPassword.Clear();
                }
                else
                {
                    MessageBox.Show("Ya hay un jugador registrado con esa cuenta de correo.\nPor favor, elegir otra cuenta o seleccionar \"Olvide mi contraseña\"");
                    txtMail.Clear();
                    txtPassword.Clear();
                    txtRepetirPassword.Clear();
                }
                
            }
        }

        bool ValidarCampos()
        {
            bool ok = false;

            if (!string.IsNullOrWhiteSpace(txtNombre.Text)&& !string.IsNullOrWhiteSpace(txtApellido.Text)&& !string.IsNullOrWhiteSpace(txtMail.Text)&& !string.IsNullOrWhiteSpace(txtUsuario.Text)&& !string.IsNullOrWhiteSpace(txtPassword.Text)&& !string.IsNullOrWhiteSpace(txtRepetirPassword.Text))
            {
                if (txtPassword.Text == txtRepetirPassword.Text)
                {
                    ok = true;
                }
                else
                {
                    ok = false;
                    MessageBox.Show("Las passwords no coinciden, por favor vuelva a ingresarlas");
                    txtPassword.Clear();
                    txtRepetirPassword.Clear();
                }
            }
            else
            {
                MessageBox.Show("Verificar campos ingresados");
                ok = false;
            }

            return ok;
        }
        
        void LimpiarCampos()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtMail.Clear();
            txtUsuario.Clear();
            txtPassword.Clear();
            txtRepetirPassword.Clear();
            txtNombre.Focus();
        }

        private void AltaJugador_Load(object sender, EventArgs e)
        {
            jugadores = Jugador.ListarTodos();
        }

        int ExisteJugador(List<Jugador> jugadores, string usuario, string mail)
        {
            int resultado = 1;

            foreach (Jugador j in jugadores)
            {
                if (j.Usuario == usuario)
                {
                    resultado = -1;
                    break;
                }
                else if (j.Mail == mail)
                {
                    resultado = -2;
                    break;
                }
            }

            return resultado;
        }
    }
}
