﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using System.Media;

namespace TP_LUG___Generala
{
    public partial class JugarPartida : Form
    {
        public JugarPartida()
        {
            InitializeComponent();
        }

        private Jugador jugador1;

        public Jugador Jugador1
        {
            get { return jugador1; }
            set { jugador1 = value; }
        }

        Random num = new Random();
        Partida partida = new Partida();
        List<Dado> dados = new List<Dado>();
        
        Jugador jugador2 = new Jugador();
        Jugador jugador = new Jugador();
        Grilla grilla1 = new Grilla();
        Grilla grilla2 = new Grilla();
        Grilla grilla = new Grilla();
        Tablero tablero = new Tablero();
        Dado dado1 = new Dado();
        Dado dado2 = new Dado();
        Dado dado3 = new Dado();
        Dado dado4 = new Dado();
        Dado dado5 = new Dado();
        SoundPlayer tirar = new SoundPlayer(".\\Recursos\\tirar.wav");
        SoundPlayer anotar = new SoundPlayer(".\\Recursos\\anotar.wav");
        SoundPlayer victoria = new SoundPlayer(".\\Recursos\\victory.wav");
        DataSet ds = new DataSet();

        private void JugarPartida_Load(object sender, EventArgs e)
        {
            tablero.EnviarCasillero += Tablero_EnviarCasillero;
            tablero.InicializarTablero();

            grilla.EnviarCasilleroGrilla += Grilla_EnviarCasilleroGrilla;
            grilla.InicializarGrilla();

            ds.ReadXml(".\\Recursos\\Tiros.xml");

            lblTurno.Text = jugador1.Nombre;

            Enlazar();

            partida.ID = Partida.InicializarPartida(jugador1.Usuario, jugador2.Usuario);
        }

        private void Grilla_EnviarCasilleroGrilla(Casillero_Grilla casilleroGrilla)
        {
            CeldaGrilla celdaGrilla = new CeldaGrilla();
            celdaGrilla.Location = new Point(casilleroGrilla.X, (casilleroGrilla.Y * 30)+25);
            celdaGrilla.Casillero = casilleroGrilla;
            celdaGrilla.EnviarPuntaje += CeldaGrilla_EnviarPuntaje;
            celdaGrilla.Turno = partida.Turno;

            this.Controls.Add(celdaGrilla);
        }

        private void CeldaGrilla_EnviarPuntaje(int puntaje, int numero)
        {
            grilla.AnotarJuego(numero);
            anotar.Play();
            AnotarPuntajeXML(numero,puntaje);
            partida.CambiarTurno();
            PasarTurno(partida.Turno);
            if(partida.TirosPartida == 22)
            {
                ContabilizarPuntos(jugador1, jugador2, partida);
                partida.RegistrarTiempo();
            }
        }

        private void Tablero_EnviarCasillero(Casillero casillero)
        {
            CeldaDado celda = new CeldaDado();
            celda.Location = new Point((casillero.X * celda.Width) + 80, casillero.Y);
            celda.Casillero = casillero;
            
            if(casillero.Numero == 1)
            {
                celda.Dado = dado1;
            }
            else if(casillero.Numero ==2)
            {
                celda.Dado = dado2;
            }
            else if (casillero.Numero == 3)
            {
                celda.Dado = dado3;
            }
            else if (casillero.Numero == 4)
            {
                celda.Dado = dado4;
            }
            else
            {
                celda.Dado = dado5;
            }

            this.Controls.Add(celda);
        }

        List<Dado> ValidarTiro(List<Dado> dados)
        {
            int resultado = 0;

            foreach (Dado d in dados)
            {
                if (!d.Tirar)
                {
                    resultado++;
                }
            }

            if (resultado == 5)
            {
                foreach (Dado d in dados)
                {
                    d.Tirar = true;
                }
            }

            return dados;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (partida.Tiro > 0)
            {
                tirar.Play();

                partida.Tiro--;

                dados= ValidarTiro(dados);

                dados = jugador.Tirar(dados, num);

                partida.EnviarPuntaje += Partida_EnviarPuntaje;

                partida.LlenarTablero(partida.ArmarVector(dados), partida.Tiro, grilla);

                MostrarDados(dados);

            }
            else
            {
                MessageBox.Show("No posee mas tiros, debe anotarse un puntaje");
            }

        }

        //para despues de tirar
        public void OcultarSeleccion()
        {
            foreach (Control c in this.Controls)
            {
                if (c is CeldaDado)
                {
                    ((CeldaDado)c).Ocultar();
                }
            }
        }

        private void Partida_EnviarPuntaje(int puntaje, int numero)
        {
            foreach (Control c in this.Controls)
            {
                if (c is CeldaGrilla && ((CeldaGrilla)c).Casillero.Numero == numero)
                {
                    ((CeldaGrilla)c).MostrarPuntaje(puntaje);
                }
            }
        }

        void Enlazar()
        {
            jugador2.Nombre = "Maquina";
            jugador2.Apellido = "Generala";
            jugador2.Usuario = "Maquina";

            dados.Add(dado1);
            dados.Add(dado2);
            dados.Add(dado3);
            dados.Add(dado4);
            dados.Add(dado5);

            anotar.Load();
            tirar.Load();
            victoria.Load();

            jugador = jugador1;
            grilla1.Jugador = jugador1;
            grilla2.Jugador = jugador2;

        }

        void PasarTurno(bool turno)
        {

            if (turno)
            {
                grilla2 = grilla;
                jugador = jugador1;
                grilla = grilla1;
                lblTurno.Text = jugador.Nombre;

            }
            else
            {
                grilla1 = grilla;
                jugador = jugador2;
                grilla = grilla2;
                lblTurno.Text = jugador.Nombre;
            }

            LimpiarTablero();
        }

        private void JugarPartida_Shown(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is CeldaDado)
                {
                    ((CeldaDado)c).EstablecerImagen(0);
                }
            }
        }

        public void MostrarDados(List<Dado> dados)
        {
            int i = 0;

            foreach (Control c in this.Controls)
            {
                if (c is CeldaDado)
                {
                    ((CeldaDado)c).EstablecerImagen(dados[i].Numero);
                    i++;
                }
                
            }
            OcultarSeleccion();
        }

        void LimpiarTablero()
        {
            foreach (Control c in this.Controls)
            {
                if (c is CeldaDado)
                {
                    ((CeldaDado)c).ReiniciarTablero();
                }
                if (c is CeldaGrilla)
                {
                    ((CeldaGrilla)c).ReiniciarGrilla();
                }
            }

            foreach (Dado d in dados)
            {
                d.Tirar = true;
            }
        }
        
        void ContabilizarPuntos(Jugador j1, Jugador j2, Partida p)
        {
            int puntaje1 = 0, puntaje2 = 0;

            foreach (Control c in this.Controls)
            {
                if (c is CeldaGrilla)
                {
                    puntaje1+= ((CeldaGrilla)c).Puntaje1;
                    puntaje2+= ((CeldaGrilla)c).Puntaje2;
                }
            }

            if (puntaje1 > puntaje2)
            {
                victoria.Play();
                partida.Tiempo = Partida.RegistrarPartida(p, j1.Usuario);
                MessageBox.Show("El Jugador Uno es el ganador!\n\nPuntaje Final:\nJugador Uno: " + puntaje1.ToString() + " - Jugador Dos: " + puntaje2.ToString() + "\n\nDuración de la partida: " + partida.Tiempo.ToString() + " minutos");
            }
            else if (puntaje2>puntaje1)
            {
                victoria.Play();
                partida.Tiempo = Partida.RegistrarPartida(p, j2.Usuario);
                MessageBox.Show("El Jugador Dos es el ganador!\n\nPuntaje Final:\nJugador Uno: " + puntaje1.ToString() + " - Jugador Dos: " + puntaje2.ToString() + "\n\nDuración de la partida: " + partida.Tiempo.ToString() + " minutos");
            }
            else
            {
                partida.Tiempo = Partida.RegistrarPartida(p, "Empate");
                MessageBox.Show("Se ha dado un empate!\n\nPuntaje Final:\nJugador Uno: " + puntaje1.ToString() + " - Jugador Dos: " + puntaje2.ToString() + "\n\nDuración de la partida: " + partida.Tiempo.ToString() + " minutos");
            }
            RegistrarJugadas();
        }

        void AnotarPuntajeXML(int numero, int puntaje)
        {
            DataRow registro = ds.Tables[0].NewRow();
            registro[0] = jugador.Usuario;
            registro[1] = ObtenerJuego(numero);
            registro[2] = puntaje;

            ds.Tables[0].Rows.Add(registro);
            ds.WriteXml(".\\Recursos\\Tiros.xml");
        }

        string ObtenerJuego(int numero)
        {
            string juego = "";

            if (numero == 1)
            {
                juego = "Uno";
            }
            else if (numero == 2)
            {
                juego = "Dos";
            }
            else if (numero == 3)
            {
                juego = "Tres";
            }
            else if (numero == 4)
            {
                juego = "Cuatro";
            }
            else if (numero == 5)
            {
                juego = "Cinco";
            }
            else if (numero == 6)
            {
                juego = "Seis";
            }
            else if (numero == 7)
            {
                juego = "Escalera";
            }
            else if (numero == 8)
            {
                juego = "Full";
            }
            else if (numero == 9)
            {
                juego = "Poker";
            }
            else if (numero == 10)
            {
                juego = "Generala";
            }
            else
            {
                juego = "Generala Doble";
            }

            return juego;
        }
    
        void RegistrarJugadas()
        {
            foreach (DataRow registro in ds.Tables[0].Rows)
            {
                Jugada j = new Jugada();
                j.IdPartida = partida.ID;
                j.Jugador = registro[0].ToString();
                j.Juego = registro[1].ToString();
                j.Puntaje = int.Parse(registro[2].ToString());

                Jugada.InsertarJugada(j, partida);
            }

        }
    }
}
