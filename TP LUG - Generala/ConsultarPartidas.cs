﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace TP_LUG___Generala
{
    public partial class ConsultarPartidas : Form
    {
        public ConsultarPartidas()
        {
            InitializeComponent();
        }
        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }

        Partida partida = null;

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConsultarPartidas_Load(object sender, EventArgs e)
        {
            var q = (from Partida p in Partida.ListarPartidas()
                     where p.Jugador1.ID == this.jugador.ID || p.Jugador2.ID == this.jugador.ID
                     select p).ToList();

            dgvPartidas.DataSource = null;
            dgvPartidas.DataSource = q;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            if ((Partida)dgvPartidas.SelectedRows[0].DataBoundItem != null)
            {
                partida = (Partida)dgvPartidas.SelectedRows[0].DataBoundItem;
                dgvJugadas.DataSource = null;
                dgvJugadas.DataSource = (from Jugada j in Jugada.ListarJugadas()
                                         where j.IdPartida == partida.ID
                                         select j).ToList();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una partida");
            }
        }
    }
}
