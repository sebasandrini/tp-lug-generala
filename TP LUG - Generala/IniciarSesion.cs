﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace TP_LUG___Generala
{
    public partial class IniciarSesion : Form
    {
        public IniciarSesion()
        {
            InitializeComponent();
        }

        List<Jugador> jugadores = new List<Jugador>();
        Jugador jugador = new Jugador();
        int intentos = 0;
        string usuario;

        public delegate void delEnviarJugador(Jugador jugador);

        public event delEnviarJugador EnviarJugador;

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {

            if (ValidarCampos())
            {
                int resultado = Jugador.ValidarJugador(txtUsuario.Text, txtContraseña.Text);

                ChequearCredenciales(resultado);
            }
            else
            {
                MessageBox.Show("Verificar datos ingresados");
            }
        }

        bool ValidarCampos()
        {
            bool ok = false;

            if (!string.IsNullOrWhiteSpace(txtUsuario.Text)&& !string.IsNullOrWhiteSpace(txtUsuario.Text))
            {
                ok = true;
            }

            return ok;
        }

        private void IniciarSesion_Load(object sender, EventArgs e)
        {
            jugadores = Jugador.ListarTodos();
            intentos = 0;
            usuario = "";
        }

        string ChequearCredenciales(int resultado)
        {
            string mensaje = "";

            if (resultado == 0)
            {
                mensaje = "El usuario se encuentra bloqueado.";
            }
            else if (resultado == -1)
            {
                mensaje = "El usuario o password son incorrectos. Vuelva a intentar.";

                if (txtUsuario.Text != usuario)
                {
                    usuario = txtUsuario.Text;
                    intentos = 0;
                }

                intentos++;

                if (intentos == 3)
                {
                    Jugador.BloquearUsuario(txtUsuario.Text);
                }
            }
            else if (resultado == -2)
            {
                mensaje = "El usuario no existe.";
            }
            else
            {
                mensaje = "Login exitoso";

                jugador = Jugador.ObtenerJugador(txtUsuario.Text);

                Sesion.InsertarSesion(jugador, mensaje);

                this.EnviarJugador(jugador);
            }

            MessageBox.Show(mensaje);

            return mensaje;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
