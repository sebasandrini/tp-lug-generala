﻿namespace TP_LUG___Generala
{
    partial class Pantalla_Inicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pantalla_Inicio));
            this.btnJugar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.jugarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jugarPartidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jugarPartidaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarEstadisticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.consultarPartidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnJugar
            // 
            this.btnJugar.Location = new System.Drawing.Point(536, 322);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(108, 59);
            this.btnJugar.TabIndex = 0;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jugarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1511, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // jugarToolStripMenuItem
            // 
            this.jugarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jugarPartidaToolStripMenuItem,
            this.registrarseToolStripMenuItem,
            this.jugarPartidaToolStripMenuItem1,
            this.consultarEstadisticasToolStripMenuItem,
            this.consultarPartidaToolStripMenuItem});
            this.jugarToolStripMenuItem.Name = "jugarToolStripMenuItem";
            this.jugarToolStripMenuItem.Size = new System.Drawing.Size(69, 24);
            this.jugarToolStripMenuItem.Text = "Partida";
            // 
            // jugarPartidaToolStripMenuItem
            // 
            this.jugarPartidaToolStripMenuItem.Name = "jugarPartidaToolStripMenuItem";
            this.jugarPartidaToolStripMenuItem.Size = new System.Drawing.Size(234, 26);
            this.jugarPartidaToolStripMenuItem.Text = "Iniciar Sesion";
            this.jugarPartidaToolStripMenuItem.Click += new System.EventHandler(this.jugarPartidaToolStripMenuItem_Click);
            // 
            // registrarseToolStripMenuItem
            // 
            this.registrarseToolStripMenuItem.Name = "registrarseToolStripMenuItem";
            this.registrarseToolStripMenuItem.Size = new System.Drawing.Size(234, 26);
            this.registrarseToolStripMenuItem.Text = "Registrarse";
            this.registrarseToolStripMenuItem.Click += new System.EventHandler(this.registrarseToolStripMenuItem_Click);
            // 
            // jugarPartidaToolStripMenuItem1
            // 
            this.jugarPartidaToolStripMenuItem1.Name = "jugarPartidaToolStripMenuItem1";
            this.jugarPartidaToolStripMenuItem1.Size = new System.Drawing.Size(234, 26);
            this.jugarPartidaToolStripMenuItem1.Text = "Jugar Partida";
            this.jugarPartidaToolStripMenuItem1.Click += new System.EventHandler(this.jugarPartidaToolStripMenuItem1_Click);
            // 
            // consultarEstadisticasToolStripMenuItem
            // 
            this.consultarEstadisticasToolStripMenuItem.Name = "consultarEstadisticasToolStripMenuItem";
            this.consultarEstadisticasToolStripMenuItem.Size = new System.Drawing.Size(234, 26);
            this.consultarEstadisticasToolStripMenuItem.Text = "Consultar estadisticas";
            this.consultarEstadisticasToolStripMenuItem.Click += new System.EventHandler(this.consultarEstadisticasToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(334, 122);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(536, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // consultarPartidaToolStripMenuItem
            // 
            this.consultarPartidaToolStripMenuItem.Name = "consultarPartidaToolStripMenuItem";
            this.consultarPartidaToolStripMenuItem.Size = new System.Drawing.Size(234, 26);
            this.consultarPartidaToolStripMenuItem.Text = "Consultar Partida";
            this.consultarPartidaToolStripMenuItem.Click += new System.EventHandler(this.consultarPartidaToolStripMenuItem_Click);
            // 
            // Pantalla_Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1511, 691);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Pantalla_Inicio";
            this.Text = "Generala LUG";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem jugarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jugarPartidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jugarPartidaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarEstadisticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarPartidaToolStripMenuItem;
    }
}

