﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace TP_LUG___Generala
{
    public partial class Estadisticas : Form
    {
        public Estadisticas()
        {
            InitializeComponent();
        }

        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }


        private void Estadisticas_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        void Enlazar()
        {
            int ganadas = 0, tiempo = 0, total=0;
            float promedio = 0;

            var q = (from Partida p in Partida.ListarPartidas()
                     where p.Jugador1.ID == this.jugador.ID || p.Jugador2.ID == this.jugador.ID
                     select p).ToList();

            dgvPartidas.DataSource = null;
            dgvPartidas.DataSource = q;

            foreach (Partida p in q)
            {
                if (p.Ganador.Usuario == jugador.Usuario)
                {
                    ganadas++;
                }
                tiempo += p.Tiempo;
                total++;
            }

            lblVictorias.Text = (ganadas / total * 100).ToString() + "%";
            lblTiempo.Text = tiempo.ToString() + "minutos jugados.";


        }

    }
}
