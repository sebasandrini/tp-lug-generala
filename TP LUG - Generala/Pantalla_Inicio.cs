﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace TP_LUG___Generala
{
    public partial class Pantalla_Inicio : Form
    {
        public Pantalla_Inicio()
        {
            InitializeComponent();
        }

        JugarPartida jugarPartida = null;
        AltaJugador altaJugador = null;
        IniciarSesion iniciarSesion = null;
        Jugador jugador = null;
        Estadisticas estadisticas = null;
        ConsultarPartidas consultarPartidas = null;
        
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (jugador != null)
            {
                jugarPartida = new JugarPartida();
                jugarPartida.MdiParent = this;
                jugarPartida.Jugador1 = jugador;
                jugarPartida.Show();
                btnJugar.Visible = false;
            }
            else
            {
                MessageBox.Show("Por favor, inicie sesión.");
            }
            
        }

        private void IniciarSesion_EnviarJugador(Jugador jugador)
        {
            pictureBox1.Visible = false;
            this.jugador = jugador;
            iniciarSesion.Close();
            jugarPartida = new JugarPartida();
            jugarPartida.Jugador1 = jugador;
            jugarPartida.MdiParent = this;
            jugarPartida.Show();
            btnJugar.Visible = false;
        }

        private void jugarPartidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            iniciarSesion = new IniciarSesion();
            iniciarSesion.MdiParent = this;
            iniciarSesion.Show();
            iniciarSesion.EnviarJugador += IniciarSesion_EnviarJugador;
            btnJugar.Visible = false;
        }

        private void registrarseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            altaJugador = new AltaJugador();
            altaJugador.MdiParent = this;
            altaJugador.Show();
            btnJugar.Visible = false;
        }

        private void jugarPartidaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (jugador != null)
            {
                jugarPartida = new JugarPartida();
                jugarPartida.MdiParent = this;
                jugarPartida.Jugador1 = jugador;
                jugarPartida.Show();
                btnJugar.Visible = false;
            }
            else
            {
                MessageBox.Show("Por favor, inicie sesión.");
            }
        }

        private void consultarEstadisticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (jugador != null)
            {
                estadisticas = new Estadisticas();
                estadisticas.MdiParent = this;
                estadisticas.Jugador = jugador;
                estadisticas.Show();
                btnJugar.Visible = false;
            }
            else
            {
                MessageBox.Show("Por favor, inicie sesión.");
            }
            
        }

        private void consultarPartidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (jugador != null)
            {
                consultarPartidas = new ConsultarPartidas();
                consultarPartidas.MdiParent = this;
                consultarPartidas.Jugador = jugador;
                consultarPartidas.Show();
                btnJugar.Visible = false;
            }
            else
            {
                MessageBox.Show("Por favor, inicie sesión.");
            }
        }
    }
}
